# chaos-array

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
> Chaos Array Assignment

## Table of Contents

- [Run](#run)
- [Maintainers](#maintainers)
- [Components](#components)
- [Contributing](#contributing)
- [License](#license)

## Run
```
Visual Studio Run
```
## Components

Chaos Array class that can add any type and will assign it a random index, can also get a random value.
Added a few extra methods for fun, as well as a recursive interactive console app

## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS