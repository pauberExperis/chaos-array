﻿using System;
using System.Collections.Generic;

namespace CustomCol
{
    class Program
    {
        public static ChaosArray<string> caString = new ChaosArray<string>();
        public static ChaosArray<int> caInt = new ChaosArray<int>();
        static void Main(string[] args)
        {
            
            /*caString.Add("lol");
            int lolNum = caString.Get("lol");
            Console.WriteLine(lolNum);
            caString.Remove(lolNum);
            Console.WriteLine(caString.Get(lolNum));
            */
            caString.Add("Jakob");
            caString.Add("Ådne");
            caString.Add("Marius");
            caString.Add("Christopher");
            caString.Add("Sindre");

            
            caInt.Add(20);
            caInt.Add(30);
            caInt.Add(40);
            caInt.Add(50);
            /*
            List<Tuple<int,string>> stringResults = caString.GetAll();
            foreach(Tuple<int,string> item in stringResults)
            {
                Console.WriteLine(item.Item1 + " " + item.Item2);
            }
            List<Tuple<int, int>> intResults = caInt.GetAll();
            foreach (Tuple<int, int> item in intResults)
            {
                Console.WriteLine(item.Item1 + " " + item.Item2);
            }
            */
            StartMethod();

        }
        public static void StartMethod()
        {
            Console.WriteLine("What type would you like to access? string/int. Press X to exit");
            string input = Console.ReadLine();
            switch(input.ToLower())
            {
                case "string":
                    StringStart();
                    break;
                case "int":
                    IntStart();
                    break;
                case "x":
                    break;
                default:
                    Console.WriteLine("Incorrect input, try again");
                    StartMethod();
                    break;

            }
        }

        public static void StringStart()
        {
            Console.WriteLine("Do you want to Add to, Remove to, GetAll, or Get an item from the list? Press X to go back");
            string input = Console.ReadLine();
            switch(input.ToLower())
            {
                case "add":
                    Console.WriteLine("What would you like to add?");
                    string input2 = Console.ReadLine();
                    caString.Add(input2);
                    Console.WriteLine(input2 + " has been added with the index of: " + caString.GetIndex(input2));
                    StartMethod();
                    break;
                case "remove":
                    Console.WriteLine("Write index of which item you'd like to remove");
                    string input3 = Console.ReadLine();
                    Console.WriteLine(caString.GetType(Int32.Parse(input3)) + " has been removed from index: " + input3);
                    caString.Remove(Int32.Parse(input3));
                    StartMethod();
                    break;
                case "getAll":
                    List<Tuple<int, string>> stringResults = caString.GetAll();
                    foreach (Tuple<int, string> item in stringResults)
                    {
                        Console.WriteLine(item.Item1 + " " + item.Item2);
                    }
                    StartMethod();
                    break;
                case "get":
                    Console.WriteLine(caString.Get()); 
                    StartMethod();
                    break;
                case "x":
                    StartMethod();
                    break;
                default:
                    Console.WriteLine("Invalid input, try again");
                    StringStart();
                    break;
            }
        }
        public static void IntStart()
        {
            Console.WriteLine("Do you want to Add to, Remove to, or Get the list? Press X to go back");
            string input = Console.ReadLine();
            switch (input.ToLower())
            {
                case "add":
                    Console.WriteLine("What would you like to add?");
                    string input2 = Console.ReadLine();
                    caInt.Add(Int32.Parse(input2));
                    Console.WriteLine(input2 + " has been added with the index of: " + caInt.GetIndex(Int32.Parse(input2)));
                    StartMethod(); 
                    break;
                case "remove":
                    Console.WriteLine("Write index of which item you'd like to remove");
                    string input3 = Console.ReadLine();
                    Console.WriteLine(caInt.GetType(Int32.Parse(input3)) + " has been removed from index: " + input3);
                    caInt.Remove(Int32.Parse(input3));
                    StartMethod();
                    break;
                case "getAll":
                    List<Tuple<int, int>> intResults = caInt.GetAll();
                    foreach (Tuple<int, int> item in intResults)
                    {
                        Console.WriteLine(item.Item1 + " " + item.Item2);
                    }
                    StartMethod();
                    break;
                case "get":
                    Console.WriteLine(caInt.Get()); 
                    StartMethod();
                    break;
                case "x":
                    StartMethod();
                    break;
                default:
                    Console.WriteLine("Invalid input, try again");
                    IntStart();
                    break;
            }
        }

    }
}
