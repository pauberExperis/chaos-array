﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCol
{
    class ChaosArray<T>
    {
        private static List<Tuple<int,T>> items = new List<Tuple<int,T>>();
        private static List<int> usedInts = new List<int>();
        private static Random rand = new Random();

        public void Add(T item)
        {
            int randomInt = rand.Next();
            bool yesnomaybe = true;
            foreach(int num in usedInts)
            {
                if (num == randomInt)
                {
                    yesnomaybe = false;
                    Add(item);
                }
            }
            if (yesnomaybe == true) {
                Tuple<int, T> insertable = new Tuple<int, T>(randomInt, item);
                items.Add(insertable);
                bool zeroExist = false;
                int zeroIndex = 0;
                for (int i = 0; i < usedInts.Count; i++)
                {
                    if (usedInts[i] == 0)
                    {
                        zeroExist = true;
                        zeroIndex = i;
                    }
                }if (zeroExist)
                {
                    usedInts[zeroIndex] = randomInt;
                }
                else
                {
                    usedInts.Add(randomInt);
                }
                    }
        }
        public void Remove(int num)
        {
            foreach(Tuple<int, T> item in items)
            {
                if (item.Item1 == num)
                {
                    items.Remove(item);
                    for (int i = 0; i < usedInts.Count; i++)
                    {
                        if (usedInts[i] == num)
                        {
                            usedInts[i] = 0;
                            break;
                        }
                    }
                    break;
                } 
            }

        }

        public int GetIndex(T type)
        {
            foreach(Tuple<int, T> item in items)
            {
                if (item.Item2.Equals(type))
                {
                    return item.Item1;
                }
            }
            return default;
        }
        public T GetType(int num)
        {
            foreach (Tuple<int, T> item in items)
            {
                if (item.Item1 == num)
                {
                    return item.Item2;
                }
            }
            return default(T);
        }

        public string Get()
        {
            string result = "";
            int randomLol = rand.Next(0, items.Count - 1);
            result = "You got item: " + items[randomLol].Item2.ToString() + " on index: " + items[randomLol].Item1.ToString() ;
            return result;
        }

        public List<Tuple<int,T>> GetAll()
        {
            List<Tuple<int, T>> results = new List<Tuple<int, T>>();
            int lowestNumber = 0;
            int secondLowestNumber = int.MaxValue;
            for(int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < items.Count; j++)
                {
                    if (items[j].Item1 < secondLowestNumber && items[j].Item1 > lowestNumber)
                    {
                        secondLowestNumber = items[j].Item1;
                    }
                }
                lowestNumber = secondLowestNumber;
                results.Add(Tuple.Create(lowestNumber,GetType(lowestNumber)));
                secondLowestNumber = int.MaxValue;
            }
            return results;
        }
    }
}
